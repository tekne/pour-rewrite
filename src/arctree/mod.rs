/*!
The `arctree` implementation of a radix trie.
*/
use crate::minimap::*;
use crate::pair::*;
use crate::radix::*;
use crate::*;
use elysees::Arc;

/// A radix trie implemented as a simple array of `N` `Arc`-backed branches. Note that `N` must be equal to `2^k` where `k` divides `8`.
#[derive(Debug, Clone)]
pub struct ArcTrie<M, const N: usize> {
    /// The branches of this trie
    branches: ArrayWrapper<Branch<ArcTrie<M, N>, M>, N>,
    /// The radix bit-depth of this trie
    depth: usize,
    /// The number of elements in this trie
    len: usize,
}

/// A hack so `IsArray` can be implemented for a generic array `[E; N]`; I should make a pull request so this is implemented for normal arrays...
#[derive(Debug, Clone)]
#[repr(transparent)]
struct ArrayWrapper<E, const N: usize>([E; N]);

unsafe impl<E, const N: usize> array_init::IsArray for ArrayWrapper<E, N> {
    type Item = E;
    #[inline]
    fn len() -> usize {
        N
    }
}

impl<M, const N: usize> Default for ArcTrie<M, N>
where
    M: Default,
{
    #[inline]
    fn default() -> ArcTrie<M, N> {
        ArcTrie {
            branches: array_init::array_init(|_| Branch::default()),
            depth: usize::MAX,
            len: 0,
        }
    }
}

/// A branch of an `Arc`-based radix trie
#[derive(Debug, Clone)]
pub enum Branch<A, M> {
    /// A minimap
    Minimap(M),
    /// A subtree, which is stored behind an `Arc`
    Subtree(Arc<A>),
}

impl<M, const N: usize> Branch<ArcTrie<M, N>, M>
where
    M: MinimapUtils,
    TyInt<N>: ValidRadixBits,
{
    /// Get the number of elements in this branch
    #[inline]
    pub fn len(&self) -> usize {
        match self {
            Branch::Minimap(map) => map.len(),
            Branch::Subtree(tree) => tree.len(),
        }
    }
    /// Get whether this branch is empty
    #[inline]
    pub fn is_empty(&self) -> bool {
        match self {
            Branch::Minimap(map) => map.is_empty(),
            Branch::Subtree(tree) => tree.is_empty(),
        }
    }
}

impl<M, const N: usize> Branch<ArcTrie<M, N>, M>
where
    M: MinimapUtils + Default,
    TyInt<N>: ValidRadixBits,
{
    /// Normalize this branch
    #[inline]
    pub fn normalize(&mut self) {
        if let Branch::Subtree(tree) = self {
            if tree.is_empty() {
                *self = Branch::default()
            }
        }
    }
}

impl<M, const N: usize> Branch<ArcTrie<M, N>, M>
where
    TyInt<N>: ValidRadixBits,
{
    /// Sample from this branch, returning a pair if it is nonempty
    #[inline]
    pub fn sample<C>(&self, ctx: &mut C) -> Option<&M::Pair>
    where
        M: SampleMinimap<C>,
    {
        match self {
            Branch::Minimap(m) => m.sample(ctx),
            Branch::Subtree(t) => t.sample(ctx),
        }
    }
}

impl<A, M> Default for Branch<A, M>
where
    M: Default,
{
    #[inline]
    fn default() -> Branch<A, M> {
        Branch::Minimap(M::default())
    }
}

impl<M, const N: usize> ArcTrie<M, N>
where
    TyInt<N>: ValidRadixBits,
{
    /// Get the byte index for a given depth
    #[inline]
    pub const fn depth_byte_index(depth: usize) -> usize {
        depth / 8
    }
    /// Get the wrapped bit index for a given depth
    #[inline]
    pub const fn depth_bit_index(depth: usize) -> u32 {
        (depth % 8) as u32
    }
    /// Get this trie's byte index
    #[inline]
    pub const fn byte_index(&self) -> usize {
        Self::depth_byte_index(self.depth)
    }
    /// Get this trie's wrapped bit index
    #[inline]
    pub const fn bit_index(&self) -> u32 {
        Self::depth_bit_index(self.depth)
    }
    /// Get the branch for a given byte at a given depth
    #[inline]
    pub const fn byte_branch(&self, byte: u8) -> &Branch<ArcTrie<M, N>, M> {
        &self.branches.0[byte.wrapping_shr(self.bit_index()) as usize % N]
    }
    /// Mutably get the branch for a given byte at a given depth
    #[inline]
    pub fn byte_branch_mut(&mut self, byte: u8) -> &mut Branch<ArcTrie<M, N>, M> {
        &mut self.branches.0[byte.wrapping_shr(self.bit_index()) as usize % N]
    }
    /// Get the branch index for a given key
    #[inline]
    pub fn branch_ix<R: RadixKey>(&self, key: &R) -> u8 {
        key.get_key_byte(self.depth).unwrap_or(0)
    }
    /// Get the branch for a given key
    #[inline]
    pub fn branch<R: RadixKey>(&self, key: &R) -> &Branch<ArcTrie<M, N>, M> {
        self.byte_branch(self.branch_ix(key))
    }
    /// Get the minimap and subtree for a given key
    #[inline]
    pub fn minimap_subtree<R: RadixKey>(&self, key: &R) -> (&Self, &M) {
        let mut curr = self;
        loop {
            curr = match curr.branch(key) {
                Branch::Minimap(map) => return (curr, map),
                Branch::Subtree(tree) => tree,
            }
        }
    }
    /// Get the length of this `ArcTrie`
    #[inline]
    pub fn len(&self) -> usize {
        self.len
    }
    /// Get whether this `ArcTrie` is empty
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }
    /// Get the depth of this `ArcTrie`
    #[inline]
    pub fn depth(&self) -> usize {
        if self.len == 0 {
            debug_assert_eq!(self.depth, usize::MAX)
        }
        self.depth
    }
    /// Get the branch array of this `ArcTrie`
    #[inline]
    pub fn branches(&self) -> &[Branch<ArcTrie<M, N>, M>; N] {
        &self.branches.0
    }
    /// Iterate over the subtrees of this `ArcTrie`
    #[inline]
    pub fn subtrees(&self) -> impl Iterator<Item = &Arc<ArcTrie<M, N>>> {
        self.branches().iter().filter_map(|branch| {
            if let Branch::Subtree(tree) = branch {
                Some(tree)
            } else {
                None
            }
        })
    }
    /// Iterate over the minimaps of this `ArcTrie`
    #[inline]
    pub fn minimaps(&self) -> impl Iterator<Item = &M> {
        self.branches().iter().filter_map(|branch| {
            if let Branch::Minimap(map) = branch {
                Some(map)
            } else {
                None
            }
        })
    }
    /// Get the subtree count of this `ArcTrie`
    #[inline]
    pub fn subtree_count(&self) -> usize {
        self.subtrees().count()
    }
    /// Get the minimap count of this `ArcTrie`
    #[inline]
    pub fn minimap_count(&self) -> usize {
        self.minimaps().count()
    }
    /// Sample from this minimap, returning a pair if it is nonempty
    #[inline]
    pub fn sample<C>(&self, ctx: &mut C) -> Option<&M::Pair>
    where
        M: SampleMinimap<C>,
    {
        // Check minimaps before subtrees, since minimaps are assumed to be faster to sample
        if let Some(sample) = self.minimaps().filter_map(|map| map.sample(ctx)).next() {
            Some(sample)
        } else {
            self.subtrees().filter_map(|tree| tree.sample(ctx)).next()
        }
    }
    /// Get the bit difference between this subtree and a radix key, or `None` if the radix key fits in this subtree
    #[inline]
    pub fn sample_bit_diff<R, C>(&self, key: &R, ctx: &mut C) -> Option<usize>
    where
        R: RadixKey,
        M: SampleMinimap<C>,
        M::Pair: RadixKey,
    {
        let sample = self.sample(ctx)?;
        let bit_diff = sample.bit_diff_ix(key)?;
        if bit_diff >= self.depth {
            None
        } else {
            Some(bit_diff)
        }
    }
    /// Create a new tree from an iterator over pairs
    #[inline]
    pub fn from_pairs<C>(pairs: impl Iterator<Item = M::Pair>, ctx: &mut C) -> ArcTrie<M, N>
    where
        M: FunMinimap<C> + Clone + Default + CloneMinimap<C>,
        M::Pair: RadixKey,
    {
        let mut new = ArcTrie::default();
        for pair in pairs {
            <Self as MutPointMap<C, M::Pair, M::Pair>>::insert_with(&mut new, pair, ctx);
        }
        new
    }
}

impl<M, C, K, const N: usize> MutPointMap<C, K, M::Pair> for Branch<ArcTrie<M, N>, M>
where
    M: FunMinimap<C> + Clone + Default + CloneMinimap<C>,
    M::Pair: KVPair<K> + RadixKey,
    K: RadixKey,
    TyInt<N>: ValidRadixBits,
{
    fn edit_with(
        &mut self,
        point: K,
        edit: impl FnOnce(K, Option<&M::Pair>, &mut C) -> Unary<Update<M::Pair, K>>,
        ctx: &mut C,
    ) -> bool {
        match self {
            Branch::Minimap(map) => match map.try_edit(point, edit, ctx) {
                Ok(change) => change,
                Err(insert) => {
                    let old = std::mem::replace(map, M::default());
                    let new =
                        ArcTrie::from_pairs(old.pop_iter(ctx).chain(std::iter::once(insert)), ctx);
                    //TODO: cons...
                    *self = Branch::Subtree(Arc::new(new));
                    true
                }
            },
            Branch::Subtree(tree) => {
                //TODO: normalization?
                if let Unary::New(new) = tree.edited_with(point, edit, ctx) {
                    //TODO: cons...
                    *tree = Arc::new(new);
                    true
                } else {
                    false
                }
            }
        }
    }
}

impl<M, C, K, const N: usize> MutPointMap<C, K, M::Pair> for ArcTrie<M, N>
where
    M: FunMinimap<C> + Clone + Default + CloneMinimap<C>,
    M::Pair: KVPair<K> + RadixKey,
    K: RadixKey,
    TyInt<N>: ValidRadixBits,
{
    fn edit_with(
        &mut self,
        point: K,
        edit: impl FnOnce(K, Option<&M::Pair>, &mut C) -> Unary<Update<M::Pair, K>>,
        ctx: &mut C,
    ) -> bool {
        if self.depth == usize::MAX && self.len != 0 {
            unimplemented!("Max depth edits")
        }
        let branch_ix = self.branch_ix(&point) as usize;
        let old_len = self.branches.0[branch_ix].len();
        let change = self.branches.0[branch_ix].edit_with(point, edit, ctx);
        if change {
            self.len -= old_len;
            self.len += self.branches.0[branch_ix].len();
            true
        } else {
            debug_assert_eq!(self.branches.0[branch_ix].len(), old_len);
            false
        }
    }
}

impl<M, C, K, const N: usize> PointMap<C, K, M::Pair> for Branch<ArcTrie<M, N>, M>
where
    M: FunMinimap<C> + Clone + Default + CloneMinimap<C>,
    M::Pair: KVPair<K> + RadixKey,
    K: RadixKey,
    TyInt<N>: ValidRadixBits,
{
    fn edited_with(
        &self,
        point: K,
        edit: impl FnOnce(K, Option<&M::Pair>, &mut C) -> Unary<Update<M::Pair, K>>,
        ctx: &mut C,
    ) -> Unary<Self> {
        match self {
            Branch::Minimap(map) => match map.try_edited(point, edit, ctx) {
                Ok(new) => new.map(Branch::Minimap),
                Err(insert) => {
                    let new: ArcTrie<M, N> = ArcTrie::from_pairs(
                        map.clone_iter(ctx).chain(std::iter::once(insert)),
                        ctx,
                    );
                    //TODO: cons...
                    Unary::New(Branch::Subtree(Arc::new(new)))
                }
            },
            //TODO: cons, normalization (?)...
            Branch::Subtree(tree) => tree
                .edited_with(point, edit, ctx)
                .map(Arc::new)
                .map(Branch::Subtree),
        }
    }
}

impl<M, C, K, const N: usize> PointMap<C, K, M::Pair> for ArcTrie<M, N>
where
    M: FunMinimap<C> + Clone + Default + CloneMinimap<C>,
    M::Pair: KVPair<K> + RadixKey,
    K: RadixKey,
    TyInt<N>: ValidRadixBits,
{
    /// Perform an edit at a given point
    #[inline]
    fn edited_with(
        &self,
        point: K,
        edit: impl FnOnce(K, Option<&M::Pair>, &mut C) -> Unary<Update<M::Pair, K>>,
        ctx: &mut C,
    ) -> Unary<Self> {
        let branch_ix = self.branch_ix(&point) as usize;
        let new = if let Unary::New(new) = self.branches.0[branch_ix].edited_with(point, edit, ctx)
        {
            new
        } else {
            return Unary::Old(());
        };
        let mut result = ArcTrie::default();
        result.depth = self.depth;
        for (ix, branch) in self.branches().iter().enumerate() {
            if branch_ix != ix {
                result.len += branch.len();
                result.branches.0[ix] = branch.clone();
            }
        }
        result.len += new.len();
        result.branches.0[branch_ix] = new;
        Unary::New(result)
    }
}

/// A type level constant integer
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct TyInt<const N: usize>([(); N]);

/// A predicate indicating the desired integer is a valid bit number for a radix trie level
pub trait ValidRadixBits {
    /// The number of bits required
    const RADIX_BITS: usize;
    /// The number of sub-bytes in a byte
    const SUB_BYTES: usize;
}

impl ValidRadixBits for TyInt<2> {
    const RADIX_BITS: usize = 1;
    const SUB_BYTES: usize = 8;
}
impl ValidRadixBits for TyInt<4> {
    const RADIX_BITS: usize = 2;
    const SUB_BYTES: usize = 4;
}
impl ValidRadixBits for TyInt<16> {
    const RADIX_BITS: usize = 4;
    const SUB_BYTES: usize = 2;
}
impl ValidRadixBits for TyInt<256> {
    const RADIX_BITS: usize = 8;
    const SUB_BYTES: usize = 1;
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn simple_u64_map() {
        let empty: ArcTrie<Single<(u64, &'static str)>, 16> = ArcTrie::default();
        assert_eq!(empty.len(), 0);
        assert!(empty.is_empty());
        assert_eq!(empty.depth(), usize::MAX);
        let one = empty.inserted_with((1, "one"), &mut ()).unwrap();
        assert_eq!(one.len(), 1);
        assert!(!one.is_empty());
        assert_eq!(one.depth(), usize::MAX);
        assert_eq!(one.sample(&mut ()), Some(&(1, "one")));
    }
}
