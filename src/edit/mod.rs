/*!
"Edits" to immutable data structures, allowing run-time operation optimizations
*/

/// A unary edit, consisting of either a result or an unchanged input
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Edit<T, I> {
    /// A new, modified result
    New(T),
    /// An unchanged input
    Old(I),
}

impl<T, I: Copy> Edit<T, I> {
    /// Get this unary result as a reference
    #[inline]
    pub fn as_ref(&self) -> Edit<&T, I> {
        match self {
            Edit::New(v) => Edit::New(v),
            Edit::Old(o) => Edit::Old(*o),
        }
    }
    /// Get this unary result as a mutable reference
    #[inline]
    pub fn as_mut(&mut self) -> Edit<&mut T, I> {
        match self {
            Edit::New(v) => Edit::New(v),
            Edit::Old(o) => Edit::Old(*o),
        }
    }
}

impl<T, I> Edit<T, I> {
    /// Unwrap this edit
    #[inline]
    pub fn unwrap(self) -> T {
        match self {
            Edit::New(v) => v,
            Edit::Old(_) => panic!(),
        }
    }
    /// Map this edit's result
    #[inline]
    pub fn map<U>(self, func: impl FnOnce(T) -> U) -> Edit<U, I> {
        match self {
            Edit::New(v) => Edit::New(func(v)),
            Edit::Old(o) => Edit::Old(o),
        }
    }
    /// Map this edit's input type
    #[inline]
    pub fn map_input<U>(self, func: impl FnOnce(I) -> U) -> Edit<T, U> {
        match self {
            Edit::New(v) => Edit::New(v),
            Edit::Old(o) => Edit::Old(func(o)),
        }
    }
    /// Get this result, or some original value
    #[inline]
    pub fn or(self, func: impl FnOnce(I) -> T) -> T {
        match self {
            Edit::New(t) => t,
            Edit::Old(o) => func(o),
        }
    }
    /// Fold results into inputs
    #[inline]
    pub fn fold_result<U>(self, func: impl FnOnce(T) -> Edit<U, I>) -> Edit<U, I> {
        match self {
            Edit::New(v) => func(v),
            Edit::Old(o) => Edit::Old(o),
        }
    }
    /// Fold inputs into results
    #[inline]
    pub fn fold_input<U>(self, func: impl FnOnce(I) -> Edit<T, U>) -> Edit<T, U> {
        match self {
            Edit::New(v) => Edit::New(v),
            Edit::Old(o) => func(o),
        }
    }
    /// Map this edit's input type through `Some`, making it ambivalent
    #[inline]
    pub fn into_amb(self) -> Edit<T, Option<I>> {
        self.map_input(Some)
    }
    /// Get this input type as a change or `None`
    #[inline]
    pub fn change(self) -> Option<T> {
        match self {
            Edit::New(v) => Some(v),
            Edit::Old(_) => None,
        }
    }
}

impl<T, I> Edit<T, Option<I>> {
    /// Disambiguate this edit's result to a given value
    #[inline]
    pub fn resolve_amb(self, default: I) -> Edit<T, I> {
        self.map_input(|amb| amb.unwrap_or(default))
    }
    /// Disambiguate this edit's result to a default value
    #[inline]
    pub fn default_amb(self) -> Edit<T, I>
    where
        I: Default,
    {
        self.map_input(Option::unwrap_or_default)
    }
}

/// A unary edit, which is either the single input to a unary operation or a new result
pub type Unary<T> = Edit<T, ()>;

impl<T> Unary<T> {
    /// The left input is unchanged
    #[inline]
    pub fn left(self) -> StrictBinary<T> {
        self.map_input(|()| Left)
    }
    /// The right input is unchanged
    #[inline]
    pub fn right(self) -> StrictBinary<T> {
        self.map_input(|()| Right)
    }
    /// An unchanged input is either left or right
    #[inline]
    pub fn ambi(self) -> Binary<T> {
        self.map_input(|()| None)
    }
}

impl<T> From<Option<T>> for Unary<T> {
    #[inline]
    fn from(opt: Option<T>) -> Unary<T> {
        match opt {
            Some(v) => Unary::New(v),
            None => Unary::Old(()),
        }
    }
}

/// The left or right inputs to a binary operation
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Side {
    /// The left input to a binary operation
    Left,
    /// The right input to a binary operation
    Right,
}

impl Side {
    /// Flip this side
    #[inline]
    pub const fn flip(self) -> Side {
        match self {
            Left => Right,
            Right => Left,
        }
    }
}

use Side::*;

/// A strict binary edit, which is either an input to a binary operation or a new result
pub type StrictBinary<T> = Edit<T, Side>;

impl<T> StrictBinary<T> {
    /// Flip this binary input
    #[inline]
    pub fn flip(self) -> StrictBinary<T> {
        self.map_input(Side::flip)
    }
}

/// A binary edit, which is either a specified input to a binary operation, an arbitrary choice of either inputs, or a new result
pub type Binary<T> = Edit<T, Option<Side>>;

impl<T> Binary<T> {
    /// Flip this binary input
    #[inline]
    pub fn flip(self) -> Binary<T> {
        self.map_input(|amb| amb.map(Side::flip))
    }
    /// Resolve ambiguous inputs to the left
    #[inline]
    pub fn left(self) -> StrictBinary<T> {
        self.resolve_amb(Left)
    }
    /// Resolve ambiguous inputs to the right
    #[inline]
    pub fn right(self) -> StrictBinary<T> {
        self.resolve_amb(Right)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn unary_and_binary() {
        let unary = Unary::<()>::Old(());
        assert_eq!(unary, None.into());
        assert_eq!(unary.change(), None);
        assert_eq!(unary.left(), StrictBinary::Old(Side::Left));
        assert_eq!(unary.right(), StrictBinary::Old(Side::Right));
        assert_eq!(unary.left().into_amb(), Binary::Old(Some(Side::Left)));
        assert_eq!(unary.right().into_amb(), Binary::Old(Some(Side::Right)));
        assert_eq!(unary.left().flip(), StrictBinary::Old(Side::Right));
        assert_eq!(unary.right().flip(), StrictBinary::Old(Side::Left));
        assert_eq!(unary.ambi(), Binary::Old(None));
        assert_eq!(unary.ambi().left(), StrictBinary::Old(Side::Left));
        assert_eq!(unary.ambi().right(), StrictBinary::Old(Side::Right));

        assert_eq!(unary.map(|()| 4), Unary::Old(()));
        assert_eq!(unary.map(|()| 5).or(|()| 4), 4);
    }
}
