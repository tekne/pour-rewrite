/*!
An interface for radix-based keys
*/

/// A radix-based key
pub trait RadixKey {
    /// Get the nth byte of this key
    ///
    /// Behaviour is unspecified if `n >= self.key_len()`
    fn key_byte(&self, n: usize) -> u8;
    /// Get the length of this key
    fn key_len(&self) -> usize;
    /// Safely get the nth byte of this key: return `None` if out of range
    #[inline]
    fn get_key_byte(&self, n: usize) -> Option<u8> {
        if n >= self.key_len() {
            None
        } else {
            Some(self.key_byte(n))
        }
    }
    /// Get the the first difference between this radix key and another, if any
    fn diff_ix(&self, other: &(impl RadixKey + ?Sized)) -> Option<usize> {
        let this_len = self.key_len();
        let other_len = other.key_len();
        let len = this_len.min(other_len);
        for ix in 0..len {
            if self.key_byte(ix) != other.key_byte(ix) {
                return Some(ix);
            }
        }
        if this_len == other_len {
            None
        } else {
            Some(len)
        }
    }
    /// Get the first bit difference between this radix key and another, if any
    fn bit_diff_ix(&self, other: &(impl RadixKey + ?Sized)) -> Option<usize> {
        let ix = self.diff_ix(other)?;
        let bit_diff = if ix < self.key_len() && ix < other.key_len() {
            let diff = self.key_byte(ix) ^ other.key_byte(ix);
            diff.leading_zeros() as usize
        } else {
            0
        };
        Some(ix * 8 + bit_diff)
    }
}

impl<R, V> RadixKey for (R, V)
where
    R: RadixKey,
{
    fn key_byte(&self, n: usize) -> u8 {
        self.0.key_byte(n)
    }
    fn key_len(&self) -> usize {
        self.0.key_len()
    }
    #[inline]
    fn get_key_byte(&self, n: usize) -> Option<u8> {
        self.0.get_key_byte(n)
    }
    fn diff_ix(&self, other: &(impl RadixKey + ?Sized)) -> Option<usize> {
        self.0.diff_ix(other)
    }
    fn bit_diff_ix(&self, other: &(impl RadixKey + ?Sized)) -> Option<usize> {
        self.0.bit_diff_ix(other)
    }
}

/// A radix-based key with a maximum key length
pub trait MaxKeyLength: RadixKey {
    /// The maximum key length of this radix key type
    const MAX_KEY_LENGTH: usize;
}

impl<R> RadixKey for [R]
where
    R: MaxKeyLength,
{
    fn key_byte(&self, n: usize) -> u8 {
        let sub_index = n % R::MAX_KEY_LENGTH;
        let index = n / R::MAX_KEY_LENGTH;
        self[index].get_key_byte(sub_index).unwrap_or(0)
    }
    fn key_len(&self) -> usize {
        self.len() * R::MAX_KEY_LENGTH
    }
}

impl RadixKey for u8 {
    #[inline]
    fn key_byte(&self, n: usize) -> u8 {
        debug_assert_eq!(n, 0, "Indexing single byte {:x}", self);
        *self
    }
    #[inline]
    fn key_len(&self) -> usize {
        1
    }
}

impl MaxKeyLength for u8 {
    const MAX_KEY_LENGTH: usize = 1;
}

impl RadixKey for u16 {
    #[inline]
    fn key_byte(&self, n: usize) -> u8 {
        self.to_be_bytes()[n]
    }
    #[inline]
    fn key_len(&self) -> usize {
        2
    }
}

impl MaxKeyLength for u16 {
    const MAX_KEY_LENGTH: usize = 2;
}

impl RadixKey for u32 {
    #[inline]
    fn key_byte(&self, n: usize) -> u8 {
        self.to_be_bytes()[n]
    }
    #[inline]
    fn key_len(&self) -> usize {
        4
    }
}

impl MaxKeyLength for u32 {
    const MAX_KEY_LENGTH: usize = 4;
}

impl RadixKey for u64 {
    #[inline]
    fn key_byte(&self, n: usize) -> u8 {
        self.to_be_bytes()[n]
    }
    #[inline]
    fn key_len(&self) -> usize {
        8
    }
}

impl MaxKeyLength for u64 {
    const MAX_KEY_LENGTH: usize = 8;
}

impl RadixKey for u128 {
    #[inline]
    fn key_byte(&self, n: usize) -> u8 {
        self.to_be_bytes()[n]
    }
    #[inline]
    fn key_len(&self) -> usize {
        16
    }
}

impl MaxKeyLength for u128 {
    const MAX_KEY_LENGTH: usize = 16;
}

impl RadixKey for str {
    #[inline]
    fn key_byte(&self, n: usize) -> u8 {
        self.as_bytes()[n]
    }
    #[inline]
    fn key_len(&self) -> usize {
        self.len()
    }
}

impl RadixKey for String {
    #[inline]
    fn key_byte(&self, n: usize) -> u8 {
        self.as_bytes()[n]
    }
    #[inline]
    fn key_len(&self) -> usize {
        self.len()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn str_diff_ix() {
        assert_eq!("hello".diff_ix("henlo"), Some(2));
        assert_eq!("hello".diff_ix("hello"), None);
    }
    #[test]
    fn u8_diff_ix() {
        let arr1: [u8; 2] = [0, 0b11100011];
        let arr2: [u8; 2] = [0, 0b11000011];
        assert_eq!(arr1.diff_ix(&arr2[..]), Some(1));
        assert_eq!(arr1.diff_ix(&arr1[..1]), Some(1));
        assert_eq!(arr1.diff_ix(&arr1[..]), None);
        assert_eq!(arr1.bit_diff_ix(&arr2[..]), Some(8 + 2));
        assert_eq!(arr1.bit_diff_ix(&arr1[..1]), Some(8));
        assert_eq!(arr1.bit_diff_ix(&arr1[..]), None);
    }
}
