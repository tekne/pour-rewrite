/*!
# pour

`pour` is an implementation of a radix trie, a data structure supporting *hash-consing*. This allows for `O(1)` cloning, and fast unions and intersections of even unrelated sets and maps. This particular implementation is designed to be highly customizable, allowing the user to pick and choose optimizations via trait implementations. Most of this complexity, however, should be irrelevant to most users, who should just use the presets.
*/
#![forbid(missing_docs, missing_debug_implementations)]

pub mod arctree;
pub mod edit;
pub mod minimap;
pub mod pair;
pub mod radix;
use edit::*;
use pair::*;

/// An object which acts as a mutable pointwise radix trie map over a given context
pub trait MutPointMap<C, K, P> {
    /// Perform an edit at a given point, mutating this particular value, and returning if a change was made
    fn edit_with(
        &mut self,
        point: K,
        edit: impl FnOnce(K, Option<&P>, &mut C) -> Unary<Update<P, K>>,
        ctx: &mut C,
    ) -> bool;
    /// Insert a given point, returning if a change was made
    fn insert_with(&mut self, point: K, ctx: &mut C) -> bool
    where
        K: Into<P>,
    {
        self.edit_with(
            point,
            |point, _old, _ctx| Edit::New(Update::Insert(point.into())),
            ctx,
        )
    }
    /// Remove a given point, returning if a change was made
    fn remove_with(&mut self, point: K, ctx: &mut C) -> bool {
        self.edit_with(
            point,
            |point, old, _ctx| {
                if old.is_none() {
                    Edit::Old(())
                } else {
                    Edit::New(Update::Remove(point))
                }
            },
            ctx,
        )
    }
}

/// An object which acts as an immutable pointwise radix trie map over a given context
pub trait PointMap<C, K, P>: Clone + MutPointMap<C, K, P> {
    /// Perform an edit at a given point
    #[inline]
    fn edited_with(
        &self,
        point: K,
        edit: impl FnOnce(K, Option<&P>, &mut C) -> Unary<Update<P, K>>,
        ctx: &mut C,
    ) -> Unary<Self> {
        let mut result = self.clone();
        if result.edit_with(point, edit, ctx) {
            Unary::New(result)
        } else {
            Unary::Old(())
        }
    }
    /// Insert a given point, returning if a change was made
    fn inserted_with(&self, point: K, ctx: &mut C) -> Unary<Self>
    where
        K: Into<P>,
    {
        self.edited_with(
            point,
            |point, _old, _ctx| Edit::New(Update::Insert(point.into())),
            ctx,
        )
    }
    /// Remove a given point, returning if a change was made
    fn removed_with(&self, point: K, ctx: &mut C) -> Unary<Self> {
        self.edited_with(
            point,
            |point, old, _ctx| {
                if old.is_none() {
                    Edit::Old(())
                } else {
                    Edit::New(Update::Remove(point))
                }
            },
            ctx,
        )
    }
}

/// An object which acts as a mergable pointwise radix trie map over a given context
pub trait MergeMap<C, P, M>: Sized {
    /// Merge this map with another map in a given context
    fn merge_with(
        &self,
        other: &M,
        merge_map: impl FnMut(&P, &P, &mut C) -> Binary<P>,
        left_map: impl FnMut(&Self, &mut C) -> Unary<Self>,
        right_map: impl FnMut(&M, &mut C) -> Unary<M>,
        ctx: &mut C,
    ) -> Binary<Self>;
}
