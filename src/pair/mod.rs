/*!
Key-value type pairing
*/

/// A type representing a key-value pair
pub trait KVPair<K> {
    /// Check whether this pair matches a given key
    fn matches_key(&self, key: &K) -> bool;
}

impl<K, V> KVPair<K> for (K, V)
where
    K: PartialEq,
{
    fn matches_key(&self, key: &K) -> bool {
        self.0.eq(key)
    }
}

impl<K, V> KVPair<(K, V)> for (K, V)
where
    K: PartialEq,
{
    fn matches_key(&self, key: &(K, V)) -> bool {
        self.0.eq(&key.0)
    }
}

/// A pair-based update: insert a pair, or remove a key
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Update<P, K> {
    /// Insert a pair
    Insert(P),
    /// Remove a key
    Remove(K),
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn basic_key_matching() {
        assert!((3, 5).matches_key(&3));
        assert!(!(4, 5).matches_key(&3));
    }
}
