/*!
A "minimap," which can be full. Used as an easily-clonable component of larger maps
*/
use crate::edit::*;
use crate::pair::*;
use std::ops::{Deref, DerefMut};

/// Minimap utilities, which should be context-independent
pub trait MinimapUtils {
    /// The key-value pair type of this minimap
    type Pair: KVPair<Self::Pair>;
    /// Get the number of items in this minimap
    fn len(&self) -> usize;
    /// Whether this minimap is empty
    #[inline]
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

/// A minimap, which is a key-value store which may be full
pub trait Minimap<C>: Sized + MinimapUtils {
    /// Get the pair associated with an item, if any, in this minimap
    fn get<K>(&self, key: &K, ctx: &mut C) -> Option<&Self::Pair>
    where
        Self::Pair: KVPair<K>;
    /// Insert an item into this minimap within a given context
    ///
    /// Return the pair back if the minimap is full
    fn try_insert(&mut self, item: Self::Pair, ctx: &mut C) -> Result<bool, Self::Pair>;
    /// Remove an item from this minimap within a given context. Return whether any change was made
    fn remove<K>(&mut self, key: &K, ctx: &mut C) -> bool
    where
        Self::Pair: KVPair<K>;
    /// Attempt to mutably edit a minimap.
    ///
    /// On success, return the old value, if any. On failure, return the pair to be inserted
    fn try_edit<K>(
        &mut self,
        key: K,
        edit: impl FnOnce(K, Option<&Self::Pair>, &mut C) -> Unary<Update<Self::Pair, K>>,
        ctx: &mut C,
    ) -> Result<bool, Self::Pair>
    where
        Self::Pair: KVPair<K>,
    {
        let pair = self.get(&key, ctx);
        match edit(key, pair, ctx) {
            Unary::New(Update::Insert(pair)) => self.try_insert(pair, ctx),
            Unary::New(Update::Remove(key)) => Ok(self.remove(&key, ctx)),
            Unary::Old(()) => Ok(false),
        }
    }
}

/// A minimap whose elements can be iterated over, consuming it
pub trait PopMinimap<C>: Minimap<C> {
    /// The type of an iterator over the elements of this minimap
    type PopIter: Iterator<Item = Self::Pair>;
    /// Iterate over the elements of this minimap
    fn pop_iter(self, ctx: &mut C) -> Self::PopIter;
}

/// A minimap, which can have a single element sampled from it, unless it is empty
pub trait SampleMinimap<C>: Minimap<C> {
    /// Sample a pair from this minimap
    fn sample(&self, ctx: &mut C) -> Option<&Self::Pair>;
}

/// A minimap whose cloned elements can be iterated over
pub trait CloneMinimap<C>: PopMinimap<C> {
    /// The type of an iterator over the cloned elements of this minimap
    type CloneIter: Iterator<Item = Self::Pair>;
    /// Iterate over the cloned elements of this minimap
    fn clone_iter(&self, ctx: &mut C) -> Self::CloneIter;
}

/// A functional minimap, which is a key-value store which may be full
pub trait FunMinimap<C>: Minimap<C> {
    /// Get this minimap, with an item inserted, in a given context
    ///
    /// Return the pair if the minimap is full
    fn inserted(&self, item: Self::Pair, ctx: &mut C) -> Result<Unary<Self>, Self::Pair>;
    /// Get this minimap, with an item removed, in a given context
    fn removed<K>(&self, item: &K, ctx: &mut C) -> Unary<Self>
    where
        Self::Pair: KVPair<K>;
    /// Attempt to edit a minimap.
    ///
    /// On success, return the new map, if any. On failure, return the pair to be inserted
    fn try_edited<K>(
        &self,
        key: K,
        edit: impl FnOnce(K, Option<&Self::Pair>, &mut C) -> Unary<Update<Self::Pair, K>>,
        ctx: &mut C,
    ) -> Result<Unary<Self>, Self::Pair>
    where
        Self::Pair: KVPair<K>,
    {
        let pair = self.get(&key, ctx);
        match edit(key, pair, ctx) {
            Unary::New(Update::Insert(pair)) => self.inserted(pair, ctx),
            Unary::New(Update::Remove(key)) => Ok(self.removed(&key, ctx)),
            Unary::Old(()) => Ok(Unary::Old(())),
        }
    }
}

/// A simple, single pair minimap
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd, Default)]
#[repr(transparent)]
pub struct Single<P>(pub Option<P>);

impl<P> Deref for Single<P> {
    type Target = Option<P>;
    #[inline(always)]
    fn deref(&self) -> &Option<P> {
        &self.0
    }
}

impl<P> DerefMut for Single<P> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Option<P> {
        &mut self.0
    }
}

impl<P> MinimapUtils for Single<P>
where
    P: KVPair<P>,
{
    type Pair = P;
    #[inline]
    fn len(&self) -> usize {
        if self.0.is_none() {
            0
        } else {
            1
        }
    }
    #[inline]
    fn is_empty(&self) -> bool {
        self.0.is_none()
    }
}

impl<C, P> Minimap<C> for Single<P>
where
    P: KVPair<P>,
{
    #[inline]
    fn get<K>(&self, key: &K, _ctx: &mut C) -> Option<&Self::Pair>
    where
        Self::Pair: KVPair<K>,
    {
        if let Single(Some(curr)) = self {
            if curr.matches_key(key) {
                Some(curr)
            } else {
                None
            }
        } else {
            None
        }
    }
    #[inline]
    fn try_insert(&mut self, item: Self::Pair, _ctx: &mut C) -> Result<bool, Self::Pair> {
        if let Single(Some(curr)) = self {
            if curr.matches_key(&item) {
                *curr = item;
                Ok(true)
            } else {
                Err(item)
            }
        } else {
            *self = Single(Some(item));
            Ok(false)
        }
    }
    #[inline]
    fn remove<K>(&mut self, key: &K, _ctx: &mut C) -> bool
    where
        Self::Pair: KVPair<K>,
    {
        if let Single(Some(curr)) = self {
            if curr.matches_key(key) {
                *self = Single(None);
                return true;
            }
        }
        false
    }
}

impl<C, P> SampleMinimap<C> for Single<P>
where
    P: KVPair<P>,
{
    #[inline]
    fn sample(&self, _ctx: &mut C) -> Option<&Self::Pair> {
        self.0.as_ref()
    }
}

impl<C, P> FunMinimap<C> for Single<P>
where
    P: KVPair<P>,
{
    #[inline]
    fn inserted(&self, item: Self::Pair, _ctx: &mut C) -> Result<Unary<Self>, Self::Pair> {
        if let Single(Some(curr)) = self {
            if !curr.matches_key(&item) {
                return Err(item);
            }
        }
        Ok(Unary::New(Single(Some(item))))
    }
    #[inline]
    fn removed<K>(&self, item: &K, _ctx: &mut C) -> Unary<Self>
    where
        Self::Pair: KVPair<K>,
    {
        if let Single(Some(curr)) = self {
            if curr.matches_key(item) {
                return Unary::New(Single(None));
            }
        }
        Unary::Old(())
    }
}

impl<P> IntoIterator for Single<P> {
    type IntoIter = <Option<P> as IntoIterator>::IntoIter;
    type Item = P;
    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<P, C> PopMinimap<C> for Single<P>
where
    P: KVPair<P>,
{
    type PopIter = <Self as IntoIterator>::IntoIter;
    #[inline]
    fn pop_iter(self, _ctx: &mut C) -> Self::PopIter {
        self.into_iter()
    }
}

impl<P, C> CloneMinimap<C> for Single<P>
where
    P: KVPair<P> + Clone,
{
    type CloneIter = <Self as IntoIterator>::IntoIter;
    #[inline]
    fn clone_iter(&self, _ctx: &mut C) -> Self::CloneIter {
        self.clone().into_iter()
    }
}
